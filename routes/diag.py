import os
import base64
import random
import string
from PIL import Image
from pathlib import Path
from flask_restful import Resource
from flask import request, json, send_file
from werkzeug.utils import secure_filename

from nwdiag import parser as n_parser, builder as n_builder, drawer as n_drawer
from actdiag import parser as a_parser, builder as a_builder, drawer as a_drawer
from seqdiag import parser as s_parser, builder as s_builder, drawer as s_drawer
from blockdiag import parser as b_parser, builder as b_builder, drawer as b_drawer


class Diag(Resource):

    valid_ext = set(['diag'])

    valid_types = [
        "png"
        , "svg"
        , "jpg"
    ]

    valid_options = [
        "antialias"
        , "nodoctype"
        , "separate"
        , "no-transparency"
    ]

    valid_encodings = [
        "file"
        , "base64"
    ]

    valid_diags = [
        "seqdiag"
        , "blockdiag"
        , "nwdiag"
        , "actdiag"
    ]

    def post(self):

        # Allow Content-Types of JSON, plain text, and file
        if request.headers['Content-Type'] == 'application/json':
            data = request.json
        elif request.headers['Content-Type'] == 'text/plain':
            data = json.loads(request.data)
        elif request.headers['Content-Type'] == 'application/x-www-form-urlencoded':
            data = request.get_json() or request.form
        elif 'file' in request.files:
            file = request.files['file']
            if file.filename == '':
                return {'error': 'No selected file'}
            if file and self.allowed_file(file.filename):
                filename = secure_filename(file.filename)
                filepath = os.path.join(self.app.config['UPLOAD_FOLDER'], filename)
                file.save(filepath)
                with open(filepath) as image:
                   data = {}
                   data['diag'] = image.read()
                os.remove(filepath)
        else:
            return {'error': 'unsupported media type'}

        return self.convert_graph(data)

    def get(self):

        data = request.args.copy()

        if 'height' in data and type(data['height']) is str:
            data['height'] = int(data['height'])

        if 'width' in data and type(data['width']) is str:
            data['width'] = int(data['width'])

        return self.convert_graph(data)

    def convert_graph(self, data):

        ##############################################################
        ## PARAMETERS REQUIRED FOR CONVERSION
        ##############################################################

        # Can't create a diagram without source
        if 'diag' not in data:
            raise ValueError('No diagram provided')
            return {'error': 'No diagram provided'}

        # Parse out diagram type
        diagtype = data['diag'].split()
        diagtype = diagtype[0]
        if diagtype not in self.valid_diags:
            raise Exception('Invalid diagram type')
            return {'error': 'Invalid diagram type'}
        data['diagtype'] = diagtype

        # Filetype
        if 'filetype' in data:
            if data['filetype'] in self.valid_types:
                if data['filetype'] == 'svg':
                    data['mimetype'] = 'image/svg+xml'
                else:
                    data['mimetype'] = 'image/' + data['filetype']
            else:
                raise Exception('Invalid filetype')
                return {'error': 'Invalid filetype'}
        else:
            data['filetype'] = 'png'
            data['mimetype'] = 'image/png'

        # Encoding
        if 'encoding' in data:
            if data['encoding'] not in self.valid_encodings:
                raise Exception('Invalid encoding')
                return {'error': 'Invalid encoding'}
        else:
            data['encoding'] = 'file'

        ##############################################################
        ## PARAMETERS OPTIONAL FOR CONVERSION
        ##############################################################

        # Check that both height and width are specified
        # if one or the other are specified
        if 'height' in data and 'width' not in data:
            return {'error': 'specified a height but not a width'}
        elif 'width' in data and 'height' not in data:
            return {'error': 'specified a width but not a height'}

        # Check that height and width are ints
        if 'height' in data and type(data['height']) is not int:
            return {'error': 'height must be a non-decimal number'}
        elif 'width' in data and type(data['width']) is not int:
            return {'error': 'width must be a non-decimal number'}

        # Check that options are all valid
        mapped_options = dict()
        if 'options' in data:
            options = data['options']
            options = options.split(',')
            for index, option in enumerate(options):
                if option in self.valid_options:
                    if option == 'separate' and data['diagtype'] != 'blockdiag':
                        return {'error': "the 'separate' option can only be used with block diagram"}
                    if option == 'nodoctype' and data['filetype'] != 'svg':
                        return {'error': "the 'nodoctype' option can only be used with svg filetype"}
                    if option == 'no-transparency' and data['filetype'] != 'png':
                        return {'error': "the 'no-transparency' option can only be used with png filetype"}
                    mapped_options[option] = True
                else:
                    return {'error': 'option '  + option + ' is not a valid option'}

        # Setup file information
        r = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(9))
        origin_filetype = 'png'                     # Default filetype
        convert_filetype = data['filetype']         # The filetype to convert to
        convert_path = r + "." + convert_filetype   # Path to converted image

        # Apply filetype
        if convert_filetype == 'svg':
            origin_filetype = 'svg'
        origin_path =  r + '.' + origin_filetype

        # Choose what parser, builder, and drawer to use
        if data['diagtype'] == 'nwdiag':
            parser =  n_parser
            builder = n_builder
            drawer = n_drawer
        elif data['diagtype'] == 'actdiag':
            parser =  a_parser
            builder = a_builder
            drawer = a_drawer
        elif data['diagtype'] == 'seqdiag':
            parser =  s_parser
            builder = s_builder
            drawer = s_drawer
        elif data['diagtype'] == 'blockdiag':
            parser =  b_parser
            builder = b_builder
            drawer = b_drawer

        tree = parser.parse_string(data['diag'])
        diagram = builder.ScreenNodeBuilder.build(tree)
        draw = drawer.DiagramDraw(
            origin_filetype
            , diagram
            , filename=origin_path
            , **mapped_options
        )
        draw.draw()

        if 'height' in data and 'width' in data:
            draw.save((data['height'], data['width']))
        else:
            draw.save()

        # seqdiag doesn't support filetypes such as jpg
        # if the requested filetype isn't a png or svg,
        # convert it and clean up
        if origin_filetype != convert_filetype:
            origin_im = Image.open(origin_path) # Open the png
            rgb_im = origin_im.convert('RGB')   # Convert colors
            rgb_im.save(convert_path)           # Save image as requested filetype

        if data['encoding'] == 'base64':
            with open(convert_path, 'br') as image:
                encoded_image = base64.b64encode(image.read())
                response = encoded_image.decode('utf8')

        # Otherwise send the file back
        if data['encoding'] == 'file':
            response = send_file(convert_path, mimetype=data['mimetype'])

        # Clean up
        origin_path = Path(origin_path)
        convert_path = Path(convert_path)

        if origin_path.is_file():
            os.remove(origin_path)

        if convert_path.is_file():
            os.remove(convert_path)

        return response


    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in self.valid_ext

