import logging
from flask import Flask
from routes.diag import Diag
from flask_cors import CORS
from flask_restful import Api


app = Flask(__name__)
CORS(app)
api = Api(app)
Diag.app = app
api.add_resource(Diag, '/')


if __name__ == '__main__':

    # Create logger with 'spam_application'
    logger = logging.getLogger('seqdiag')
    logger.setLevel(logging.DEBUG)

    # Create file handler which logs even debug messages
    fh = logging.FileHandler('/var/log/flask_api_out.log')
    fh.setLevel(logging.DEBUG)

    # Create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    # Create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    # Add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)

    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )
