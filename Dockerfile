FROM python:3.6

MAINTAINER mlittrell@gmail.com

RUN set -x \
    # Upgrade pip
    && pip install --upgrade pip \
    # Install Flask API
    && pip install \
        Pillow \
        flask-restful \
        # Install diagram conversion tools
        seqdiag \
        blockdiag \
        actdiag \
        nwdiag \
    && pip install -U flask-cors

# Setup application
COPY . /code
WORKDIR /code
EXPOSE 80

# Start application
ENTRYPOINT ["python", "api.py"]
