# Introduction

Inspired by and built upon [blockdiag](http://blockdiag.com/en/blockdiag/).   

# Setup

```
docker-compose up -d --build
```

# Examples   
[Sequence Diagram](https://codepen.io/mlittrell/pen/BZRjNb)   
[Block Diagram](https://codepen.io/mlittrell/pen/WOOjWE)   
[Activity Diagram](https://codepen.io/mlittrell/pen/jwwmjY)   
[Network Diagram](https://codepen.io/mlittrell/pen/LLjVXa)   

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/351785e109c52fa1148a)   
